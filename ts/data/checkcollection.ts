import * as plugins from '../ul-interfaces.plugins.js';
import * as search from './search.js';

import * as checks from './checks/index.js';

export interface ICheckCollection {
  id: string;
  intervalMs: number;
  assumptionChecks?: Array<checks.IAssumptionCheck>;
  functionChecks: Array<checks.IFunctionCheck>;
  pwaChecks?: Array<checks.IPwaCheck>;
  pageRankChecks: Array<checks.IPageRankCheck>;
}
