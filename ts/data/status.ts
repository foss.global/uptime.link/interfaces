import * as plugins from '../ul-interfaces.plugins.js';

export interface IStatus {
  last90days: IDailyStatus[];
}

export interface IDailyStatus {
  timezone: 'UTC';
  date: plugins.tsclass.general.IDate;
  overallStatus: 'ok' | 'reduced' | 'outage';
  incidentRefs: string[];
}
