import * as plugins from '../ul-interfaces.plugins.js';

export interface IIncident {
  timestamp: number;
  firstResponseTimestamp?: number;
  /**
   * indicates
   */
  status: 'discovered' | 'investigating' | 'fixing' | 'fixImplemented' | 'watching' | 'resolved';
  creationMode: 'monitor' | 'manual';
  assignedUserId?: string;
  postMortemLink?: string;

  updates: {
    markdownText: string;
    type: 'comment' | 'manualUpdate' | 'automaticUpdate';
  }[];

  justForLooks: {
    isoTimestamp: string;
  };
}
